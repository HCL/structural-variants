package NGS::SV::Position;
use Moose;
use namespace::sweep;
use NGS::SV::Types;
use List::Util qw(max);

with (
    'NGS::SV::Roles::Comparator',
    );

has 'chr' => (
    is   => 'ro',
    isa  => 'NGS::SV::Chromosome',
    required => 1,
    );

for my $pos ('beg', 'end') {
    has $pos => (
	is   => 'ro',
	isa  => 'NGS::SV::Chromosome::Position',
	required => 1,
	);
}

sub width {
    my $self=shift;
    return $self->end - $self->beg,
}

has '_window' => (
    traits => ['Array'],
    is   => 'ro',
    isa  => 'ArrayRef[NGS::SV::Chromosome::Position]',
    lazy => 1,
    init_arg => undef,
    builder => '_build_window',
    handles   => {
	'window' => 'elements',
	'interval' => 'elements',
    },
    );
    

sub _build_window {
    my $self=shift;
    my $extend = 250;
    return [max ($self->beg-$extend, 0), $self->end+$extend];
}

sub startWindow {
    my $self=shift;
    return ($self->window)[0];
}

sub endWindow {
    my $self=shift;
    return ($self->window)[1];
}

sub cmp {
    my $self=shift;
    my $other=shift;
    my ($p1, $p2) = ($self, $other);

    my ($b1, $e1) = $p1->interval;
    my ($b2, $e2) = $p2->interval;

    if ($e1 < $b2) {
	return -1;
    }
    if ($e2 < $b1) {
	return 1;
    }
    return 0;
}
BEGIN { *cmpInterval = \&cmp; }

sub to_string {
    my $self=shift;

    return $self->chr."[".$self->beg.", ".$self->end."]";
}

use overload '""' => 'to_string', fallback => 1;

1;
